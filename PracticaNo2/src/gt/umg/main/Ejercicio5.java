package gt.umg.main;

import java.util.Scanner;

/**
 *
 * @author Jezer Carrillo
 */
public class Ejercicio5 {

    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);

        int x = 0;
        int ecuacion;

        System.out.println("f(x,y) = x + 3y * x^2");

        System.out.println("Ingrese el valor de X");
        x = entrada.nextInt();

        ecuacion = x * x * 3;

        System.out.println(ecuacion + "y" + " + " + x);

    }

}
