package gt.umg.main;

/**
 *
 * @author Jezer Carrillo
 */
public class Ejercicio8while {

    public static void main(String[] args) {
        int numero = 1;
        int limite = 10000;
        while (numero <= limite) {
            if (numero % 2 == 0) {
                System.out.println(numero);
            }

            numero++;

        }
    }
}
