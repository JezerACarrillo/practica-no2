package gt.umg.main;

import java.util.Scanner;

/**
 *
 * @author Jezer Carrillo
 */
public class Ejercicio2 {

    public static void main(String[] args) {

        Scanner entrada = new Scanner (System.in);
        
        System.out.println("Programa que calcula el sueldo de un empleado");
        
        int importe=0,bono=0,descuento=0,sueldo=0;
        double sueldoiva=0,iva=1.11;
        
        System.out.println("Matricula:");
        int matricula = entrada.nextInt();
        
        System.out.println("Empleado:");
        String empleado = entrada.next();
        
        System.out.println("Tipo de empleado:");
        int tipoe = entrada.nextInt();
        
        System.out.println("Dias Trabajados:");
        int diast = entrada.nextInt();
        
        System.out.println("Retardos:");
        int retardos = entrada.nextInt();
                
        System.out.println("Horas extras:");
        int horasextra = entrada.nextInt();
        
        if(tipoe==1){
            importe=350*diast;
        }
        if(tipoe==2){
            importe=250*diast;
        }
        if(tipoe==3){
            importe=125*diast;
        }
        if(horasextra>=1 && horasextra<=5){
            bono=horasextra*100;
        }
        if(horasextra>=6 && horasextra<=10){
            bono=500+((horasextra-5)*150);
        }
        if(horasextra>=11 && horasextra<=15){
            bono=1250+((horasextra-10)*275);
        }
        if(retardos>=1 && retardos<=5){
            descuento=retardos*50;
        }
        if(retardos>=6 && retardos<=10){
            descuento=250+((retardos-5)*75);
        }
        if(retardos>=11 && retardos<=15){
            descuento=625+((retardos-10)*100);
        }
        sueldo=(importe+bono)-descuento;
        sueldoiva=sueldo*iva;
        String imprimir="";
        System.out.println("Matricula: "+matricula);
        System.out.println("Empleado: "+empleado);
        System.out.println("Tipo de Empleado: "+tipoe);
        System.out.println("Dias Trabajados: "+diast);
        System.out.println("Retardos: "+retardos);
        System.out.println("Horas Extras: "+horasextra);
        System.out.println("Importe: "+importe);
        System.out.println("Bono: "+bono);
        System.out.println("Descuento: "+descuento);
        System.out.println("Sueldo: "+sueldo);
        System.out.println("Sueldo con IVA: "+sueldoiva);
                
    }

}
