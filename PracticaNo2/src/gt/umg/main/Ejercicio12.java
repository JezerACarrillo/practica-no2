package gt.umg.main;

/**
 *
 * @author Jezer Carrillo
 */
public class Ejercicio12 {

    public static boolean esPrimo(int numero) {
        return !new String(new char[numero]).matches(".?|(..+?)\\1+");
    }

    public static void main(String[] args) {
        int i;
        for (i = 2; i <= 10; i++) {
            if (esPrimo(i)) {
                System.out.println(i + " ");
            }
        }

    }

}
