package gt.umg.main;

import java.util.Scanner;

/**
 *
 * @author Jezer Carrillo
 */
public class Ejercicio13 {

    Scanner entrada = new Scanner(System.in);

    long resultadoOperacion = 0;

    int operando1 = 0;
    int operando2 = 0;

    public void ingresarOperados() {
        System.out.println("Por favor ingrese el Operando 1: ");
        operando1 = entrada.nextInt();
        System.out.println("Por favor ingrese el Operando 2: ");
        operando2 = entrada.nextInt();
    }

    public long sumar() {
        int resultadoSuma = operando1 + operando2;
        return resultadoSuma;
    }

    public long restar() {
        int resultadoResta = operando1 - operando2;
        return resultadoResta;
    }

    public long multiplicacion() {
        int resultadoMultiplicacion = operando1 * operando2;
        return resultadoMultiplicacion;
    }

    public long division() {
        int resultadoDivision = operando1 / operando2;
        return resultadoDivision;
    }

    public void iniciarMiCalculadora() {
        System.out.println("Bienvenido a mi primer Calculadora");
        System.out.println("Seleccione una opcion");
        System.out.println("1 - Sumar");
        System.out.println("2 - Restar");
        System.out.println("3 - Multiplicar");
        System.out.println("4 - Salir");

        int operacionSeleccionada = entrada.nextInt();

        if (operacionSeleccionada == 1) {
            ingresarOperados();
            resultadoOperacion = sumar();
            System.out.println("El resulatado de la suma es: " + resultadoOperacion);

        }
        if (operacionSeleccionada == 2) {
            ingresarOperados();
            resultadoOperacion = restar();
            System.out.println("El resulatado de la resta es: " + resultadoOperacion);

        }
        if (operacionSeleccionada == 3) {
            ingresarOperados();
            resultadoOperacion = multiplicacion();
            System.out.println("El resulatado de la suma es: " + resultadoOperacion);

        }
        if (operacionSeleccionada == 4) {
            ingresarOperados();
            resultadoOperacion = division();
            System.out.println("El resulatado de la suma es: " + resultadoOperacion);

        }
    }

}
