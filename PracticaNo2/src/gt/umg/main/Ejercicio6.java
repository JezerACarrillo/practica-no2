package gt.umg.main;

import java.util.Scanner;

/**
 *
 * @author Jezer Carrillo
 */
public class Ejercicio6 {

    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);

        double fahrenheit = 0;
        double conversion;
        int i = 0;

        while (true) {

            System.out.println("Ingrese los grados Fahrenheit");
            fahrenheit = entrada.nextDouble();

            conversion = (fahrenheit - 32) * 5 / 9;

            if (i <= 999) {
                System.out.println("Los grados celsius son: " + conversion);
            }
            i++;
        }

    }

}
