package gt.umg.main;

import java.util.Scanner;

/**
 *
 * @author Jezer Carrillo
 */
public class Ejercicio4 {

    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);

        double cambio = 0;
        
        System.out.println("Bienvenido a los cálculos de conversión de moneda");

        System.out.println("Ingrese la cantidad a convertir de quetzales");
        double quetzal = entrada.nextDouble();

        // elejir la moneda
        System.out.println("Seleccione moneda a cambiar");
        System.out.println("1- Equivalente en Dólares ");
        System.out.println("2- Equicalente en Euros");
        System.out.println("3- Equivalente en Franco Suizo");

        int opcion = entrada.nextInt();
        // esto es para que te muestre el nombre de la moneda que seleccionaste
        if (opcion == 1) {

            System.out.println("1- Cantidad en Dólares ");
            cambio = quetzal * 0.13;

            System.out.println("Su cambio es: " + cambio + " dólares");

        }
        if (opcion == 2) {

            System.out.println("2- Cantidad en Euros");
            cambio = quetzal * 0.11;

            System.out.println("Su cambio es: " + cambio + " euros");

        }
        if (opcion == 3) {

            System.out.println("3- Cantidad en Franco Suizo");
            cambio = quetzal * 0.13;

            System.out.println("Su cambio es: " + cambio + " francos");

        }// else {

           // System.out.println("La moneda elegida es invalida");

        //}

    }

}